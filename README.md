# README #

The task is about filling up a 5 x 5 square gamefield with numbers from 1 to 25 moving 
around like a knight in Chess. 


### Task is to write a program in Java which ###

* prints out all possible solutions (completed gamefields) if there are any
* calculates the number of solutions
* calculate how many different ways to fill the field exist (completed and incompleted 
gamefields)

### Who do I talk to? ###

* Repo owner - Igor Soroka <igr.srk@gmail.com>