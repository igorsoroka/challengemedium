package org.greng;

/**
 * Shows the coordinates in more convenient format.
 * Created by Igor Soroka<igr.srk@gmail.com> on 10/13/16.
 */
public class Coordinate {
    /**
     * x and y components
     */
    private int x;
    private int y;

    /**
     * Constructor for the class
     * @param x coordinate
     * @param y coordinate
     */
    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Getter for x
     * @return x component
     */
    public int getX() {
        return x;
    }

    /**
     * Getter for y
     * @return y component
     */
    public int getY() {
        return y;
    }
}
