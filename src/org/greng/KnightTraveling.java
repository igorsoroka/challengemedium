package org.greng;

import java.util.*;

/**
 * This class is for creating Knight object which will travel from start point
 * to search the path through specified chess board.
 * Created by Igor Soroka<igr.srk@gmail.com> on 10/10/16.
 */

public class KnightTraveling {

    private static int unvisited = -1;

    /**
     * Height of the chessboard
     */
    private int maxRow;
    /**
     * Width of the chessboard
     */
    private int maxCol;

    /**
     * Quantity of the solutions
     */
    private int solutionNum;

    /**
     * The considered chess board with maxRow x maxCol dimensions
     */
    private int [][] board;
    private int boardArea;

    /**
     * This is a constructor for the movement of knight around the board.
     * @param maxRow determines maximum number of rows of a board
     * @param maxCol determines maximum number of columns of a board
     */
    public KnightTraveling(int maxRow, int maxCol) {

        solutionNum = 0;

        this.maxRow = maxRow;
        this.maxCol = maxCol;

        this.board = new int[maxRow][maxCol];
        this.board = boardInitialization(maxRow, maxCol);
        this.boardArea = maxRow*maxCol;
    }


    /**
     * This method serves for creating the board with unvisited cells ('0').
     * @param maxRow - maximum rows number
     * @param maxCol - maximum columns number
     * @return - filled array with zeros.
     */
    private int[][] boardInitialization(int maxRow, int maxCol) {
        int[][] arr = new int[maxRow][maxCol];
        for (int i = 0; i < this.maxRow; i++) {
            for(int j = 0; j < this.maxCol; j++) {
                arr[i][j] = unvisited;
            }
        }
        return arr;
    }

    /**
     * Method returns all possible movements for one cell
     * @param x current coordinate
     * @param y current coordinate
     * @return list with the coordinates of possible moves
     */
    private List<Coordinate> getPossibleMoves(int x, int y) {
        List<Coordinate> l = new ArrayList<>();
        if (x + 2 < maxRow && y - 1 >= 0)
            l.add(new Coordinate(x + 2, y - 1)); //right and upward
        if (x + 1 < maxRow && y - 2 >= 0)
            l.add(new Coordinate(x + 1, y - 2)); //upward and right
        if (x - 1 >= 0 && y - 2 >= 0)
            l.add(new Coordinate(x - 1, y - 2)); //upward and left
        if (x - 2 >= 0 && y - 1 >= 0)
            l.add(new Coordinate(x - 2, y - 1)); //left and upward
        if (x - 2 >= 0 && y + 1 < maxCol)
            l.add(new Coordinate(x - 2, y + 1)); //left and downward
        if (x - 1 >= 0 && y + 2 < maxCol)
            l.add(new Coordinate(x - 1, y + 2)); //downward and left
        if (x + 1 < maxRow && y + 2 < maxCol)
            l.add(new Coordinate(x + 1, y + 2)); //downward and right
        if (x + 2 < maxRow && y + 1 < maxCol)
            l.add(new Coordinate(x + 2, y + 1)); //right and downward
        return l;
    }

    /**
     * Solve the problem of going through cells
     */
    public void determine(int startRow, int startCol) {
        searchForPath(startRow, startCol, 0);
        this.board[startRow][startCol] = unvisited;
    }

    /**
     * Going through the chessboard
     * @param x current coordinate
     * @param y current coordinate
     * @param turnNumber number of move
     */
    private void searchForPath(int x, int y, int turnNumber) {
        board[x][y] = turnNumber;
        if (turnNumber == this.boardArea-1) {
            solutionNum++;
            printResultMoves();
        } else {
            for (Coordinate coord : getPossibleMoves(x, y)) {
                if (this.board[coord.getX()][coord.getY()] == unvisited) {
                    searchForPath(coord.getX(), coord.getY(), turnNumber+1);
                    //reset the cell
                    this.board[coord.getX()][coord.getY()] = unvisited;
                }
            }
        }


    }

    /**
     * Printing the resultant board with the numbers inside each 'cell', which represents the sequence of
     * knight moves.
     */
    private void printResultMoves() {
        System.out.println("Solution #" + solutionNum);
        for (int k = 0; k < this.maxRow; k++) {
            for (int l = 0; l < this.maxCol; l++) {
                System.out.print(" " + String.format("%02d", this.board[k][l]+1) + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public int getSolutionNum() {
        return solutionNum;
    }
}
