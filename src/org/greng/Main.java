package org.greng;

import java.util.Scanner;

/**
 * Main Class for a problem of Knight Travelling around the chessboard by L-moves without repeat
 * Created by Igor Soroka<igr.srk@gmail.com> 10.10.2016
 */
public class Main {

    /**
     * Main method to run console application
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

            while (true) {
                try {
                    System.out.println("***********");
                    System.out.println("The task is about filling up a\n" +
                            "5 x 5 square game field with numbers from 1 to 25 moving \n" +
                            "around like a knight in Chess.");

                    printInitialBoard();
                    System.out.print("Choose initial X position (1-5): ");
                    int choiceX = sc.nextInt();
                    System.out.print("Choose initial Y position (1-5): ");
                    int choiceY = sc.nextInt();

                    KnightTraveling kt = new KnightTraveling(5, 5);
                    if (choiceX >= 1 && choiceX <= 5 && choiceY >= 1
                            && choiceY <= 5 && choiceX == (int) choiceX && choiceY == (int) choiceY) {
                        kt.determine(choiceX - 1, choiceY - 1);
                        System.out.println("***** Completed Solutions found: " + kt.getSolutionNum() + " *****");
                    }
                    else {
                        System.out.println("Input in wrong format!");
                        break;
                    }

                    System.out.println("Start with another position? (y/n)");
                    String choice = sc.next();
                    if (choice.contains("n")) {
                        break;
                    }
                } catch (Exception e) {
                    System.out.println("Repeat the input!");
                    break;
                }

            }
    }

    /**
     * Printing board filled with squares
     */
    private static void printInitialBoard() {
        for (int k = 0; k < 5; k++) {
            for (int l = 0; l < 5; l++) {
                System.out.print(" " + "\u25A1" + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}